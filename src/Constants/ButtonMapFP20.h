#pragma once

#include "../Enums/ButtonIndexFP20.h"
#include "../Registers.h"

namespace mas
{
  // Which port pins map to which button indexes?
  const uint8_t BUTTON_PORT_PIN_MAP_FP20[PORT_COUNT][8] =
  {
      // PORT_A
      {
        BUTTON_PORT_PIN_NOT_MAPPED,                         // PA0 | 
        BUTTON_PORT_PIN_NOT_MAPPED,                         // PA1 | 
        BUTTON_PORT_PIN_NOT_MAPPED,                         // PA2 | 
        static_cast<uint8_t>(ButtonIndexFP20::MenuOk),      // PA3 | Button: Menu/Ok
        static_cast<uint8_t>(ButtonIndexFP20::PlayPause),   // PA4 | Button: Play/Pause
        static_cast<uint8_t>(ButtonIndexFP20::Prev),        // PA5 | Button: Prev
        static_cast<uint8_t>(ButtonIndexFP20::Next),        // PA6 | Button: Next
        static_cast<uint8_t>(ButtonIndexFP20::FolderBack),  // PA7 | Button: Folder/Back
      },

      // PORT_B
      {
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB0 | Encoder: Left A
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB1 | Encoder: Left B
          static_cast<uint8_t>(ButtonIndexFP20::EncLeft),   // PB2 | Button: Encoder Left
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB3 | 
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB4 | 
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB5 | MOSI
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB6 | MISO
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB7 | SCK
      },

      // PORT_C
      {
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC0 | SCL
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC1 | SDA
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC2 | Encoder: Right A
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC3 | Encoder: Right B
          static_cast<uint8_t>(ButtonIndexFP20::EncRight),  // PC4 | Button: Encoder Right
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC5 | OLED: CS
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC6 | OLED: DC
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC7 | OLED: Reset
      },

      // PORT_D
      {
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD0 | Main: RX (RXD0)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD1 | Main: TX (TXD0)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD2 | VFD: RX (RXD1)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD3 | VFD: TX (TXD1)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD4 | VFD: SBusy
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD5 | VFD: Reset
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD6 | 
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD7 | 
      },
  };
}
