#pragma once

#include "../Enums/ButtonIndexFP10.h"
#include "../Registers.h"

namespace mas
{
  // Which port pins map to which button indexes?
  const uint8_t BUTTON_PORT_PIN_MAP_FP10[PORT_COUNT][8] =
  {
      // PORT_A
      {
          static_cast<uint8_t>(ButtonIndexFP10::P6Folder),  // PA0 | Button: P6Folder
          static_cast<uint8_t>(ButtonIndexFP10::Next),      // PA1 | Button: Next
          static_cast<uint8_t>(ButtonIndexFP10::Prev),      // PA2 | Button: Prev
          static_cast<uint8_t>(ButtonIndexFP10::P5Repeat),  // PA3 | Button: P5Repeat
          static_cast<uint8_t>(ButtonIndexFP10::P4Shuffle), // PA4 | Button: P4Shuffle
          static_cast<uint8_t>(ButtonIndexFP10::P3Stop),    // PA5 | Button: P3Stop
          static_cast<uint8_t>(ButtonIndexFP10::P2Pause),   // PA6 | Button: P2Pause
          static_cast<uint8_t>(ButtonIndexFP10::P1Play),    // PA7 | Button: P1Play
      },

      // PORT_B
      {
          static_cast<uint8_t>(ButtonIndexFP10::EncLeft),   // PB0 | Button: Encoder Left
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB1 | Encoder: Left A
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB2 | Encoder: Left B
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB3 | LED: P6/Folder
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB4 | LED: P5/Repeat
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB5 | Display: 0 (MOSI)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB6 | Display: 1 (MISO)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PB7 | Display: 2 (SCK)
      },

      // PORT_C
      {
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC0 | Display: 3 (SCL)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC1 | Display: 4 (SDA)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PC2 | Display: 5
          static_cast<uint8_t>(ButtonIndexFP10::Display),   // PC3 | Button: Display
          static_cast<uint8_t>(ButtonIndexFP10::Mute),      // PC4 | Button: Mute
          static_cast<uint8_t>(ButtonIndexFP10::MenuOk),    // PC5 | Button: Menu
          static_cast<uint8_t>(ButtonIndexFP10::Back),      // PC6 | Button: Back
          static_cast<uint8_t>(ButtonIndexFP10::EncRight),  // PC7 | Button: Encoder Right
      },

      // PORT_D
      {
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD0 | RXD0
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD1 | TXD0
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD2 | Encoder: Right A (RXD1)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD3 | Encoder: Right B (TXD1)
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD4 | LED: P1/Play
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD5 | LED: P2/Pause
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD6 | LED: P3/Stop
          BUTTON_PORT_PIN_NOT_MAPPED,                       // PD7 | LED: P4/Shuffle
      },
  };
}
