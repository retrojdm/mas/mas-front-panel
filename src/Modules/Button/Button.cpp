#include "Button.h"

namespace mas
{
  // Class constructor /////////////////////////////////////////////////////////////////////////////////////////////////

  Button::Button(Broker<Message *> & broker, uint8_t index)
    : Publisher(broker),
    index(index),
    _rawState(),
    _debouncedState(),
    _debounced(),
    _debounceTimer(),
    _held(),
    _holdTimer()
  {
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  void Button::loop()
  {
    bool up = (_debouncedState & 0B01) == 0;
    if ((up) || (_held) || (_holdTimer == 0))
    {
      return;
    }

    if (millis() - _holdTimer >= BUTTON_HELD_DURATION)
    {
      _held = true;    
      publish(new(std::nothrow) ButtonHeldEvent(index));
    }
  }

  void Button::setState(uint8_t rawIsDown)
  {
    // The button's 'state' is a 2-bit value.
    // bit 1: wasDown
    // bit 0: isDown
    _rawState <<= 1;
    _rawState &= 0B11;
    _rawState |= rawIsDown;

    unsigned long now = millis();

    // reset debounce timer on rising and falling edge.
    if ((_rawState == 0B01) || (_rawState == 0B10))
    {
      _debounceTimer = now;
      _debounced = false;
    }

    if ((now - _debounceTimer < BUTTON_DEBOUNCE_DURATION) || _debounced)
    {
      return;
    }

    // Take the raw isDown bit, and shift it into the debounced state.
    _debouncedState <<= 1;
    _debouncedState &= 0B11;
    _debouncedState |= rawIsDown;
    _debounced = true;

    // if (!wasDown && isDown)
    if (_debouncedState == 0B01)
    {
      _holdTimer = now;
      _held = false;
      publish(new(std::nothrow) ButtonDownEvent(index));
      return;
    }

    // if (wasDown && !isDown)
    if (_debouncedState == 0B10)
    {
      publish(new(std::nothrow) ButtonUpEvent(index));
      
      if (!_held)
      {
        _holdTimer = 0;
        _held = false;
        publish(new(std::nothrow) ButtonPressedEvent(index));
        return;
      }

      _holdTimer = 0;
      _held = false;
    }
  }
}
