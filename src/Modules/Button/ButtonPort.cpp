#include "ButtonPort.h"

namespace mas
{
  // Class constructors ////////////////////////////////////////////////////////////////////////////////////////////////

  ButtonPort::ButtonPort()
  {
  }
  
  void ButtonPort::begin(uint8_t port, const uint8_t buttonMap[8], Button *buttons)
  {
    _port = port;

    _pDDRx  = DDRx[port];
    _pPINx  = PINx[port];
    _pPORTx = PORTx[port];

    _enabled = false;
    
    // Convert button index map to array of pointers to button objects.
    for (uint8_t i = 0; i < 8; i++)
    {
      uint8_t index = buttonMap[i];
      _pButtonMap[i] = (index != BUTTON_PORT_PIN_NOT_MAPPED) ? &(buttons[index]) : nullptr;
    }

    // Bit values: 1 = mapped as inputs. 0 = not mapped.
    uint8_t mask = 0;
    for (uint8_t i = 0; i < 8; i++)
    {
      bool mapped = (_pButtonMap[i] != nullptr);
      mask >>= 1;
      mask |= (mapped ? 0B10000000 : 0);
    }

    // Optimisation to skip port if no buttons are mapped to it.
    _enabled = mask > 0;
        
    *_pDDRx &= ~mask; // Set buttons as inputs
    *_pPORTx |= mask; // Set pullup resistors
  }

  // Read a byte from the port, then shift through each bit. If the bit position maps to a button, set that button's
  // state.
  void ButtonPort::loop()
  {
    if (!_enabled)
    {
      return;
    }

    uint8_t value = *_pPINx;
    for (uint8_t i = 0; i < 8; i++)
    {
      Button *pButton = _pButtonMap[i];
      if (pButton != nullptr)
      {
        // Reading a logic 0 means the button is grounded. We invert this value so that a logic 1 represents "isDown"
        // for our call to .setState().
        //
        // We bitwise AND the inverted value, to get only the least significant bit.
        uint8_t isDown = (~value & 1);
        pButton->setState(isDown);
      }
      value >>= 1;
    }
  }
}
