#pragma once

#include <Arduino.h>
#include <PubSub.h>
#include <MasCore.h>

namespace mas
{
  // Button
  // ======
  // 
  // The button object is used to keep track of a button's state. It doesn't deal with the physical IO pins at all.
  // Instead, it's setState() method is called by the ButtonPort object.
  //
  // Depending on how the state changes, it publishes "down", "pressed", or "held" events.

  // Constants /////////////////////////////////////////////////////////////////////////////////////////////////////////

  // How long should an input settle for before considering it's state as changed?
  const uint32_t BUTTON_DEBOUNCE_DURATION = 10; // millis

  // How long do we need to hold a button before a "held" event is generated?
  const uint32_t BUTTON_HELD_DURATION = 500; // millis

  // Class definition //////////////////////////////////////////////////////////////////////////////////////////////////

  class Button : public pubsub::Publisher<Message *>
  {
  public:
    Button(Broker<Message *> & broker, uint8_t index);
    void loop();
    void setState(const uint8_t isDown);

  	uint8_t index;

  private:
  	// bit 1: previous state
  	// bit 0: current state
    uint8_t   _rawState;
    uint8_t   _debouncedState;

    bool      _debounced;
    uint32_t  _debounceTimer;
    
    bool      _held;
    uint32_t  _holdTimer;
  };
}
