#include "ButtonModule.h"

namespace mas
{
  // Class constructors ////////////////////////////////////////////////////////////////////////////////////////////////

  ButtonModule::ButtonModule(
    Broker<Message *> & broker,
    char* const errorBuffer,
    const uint8_t (*buttonPortPinMap)[8],
    const uint8_t buttonCount)
    :
    ModuleBase(broker, Module::BUTTON_KEY, errorBuffer),
    _buttonPortPinMap(buttonPortPinMap),
    _buttonCount(buttonCount),
    _buttonPorts(),
    _buttons({
      // C++ forces us to construct all buttons here, since they don't use the a default constructor.
      // We won't be using all of them. Eg: The FP10 has 14 buttons, and th FP20 has only 7.
      //
      // The button objects used in this array will be sequential from 0..n so that we can use a simple for loop to 
      // process them. They DON'T map to the pin locations on the ports.
      //
      // The index passed here is used when emitting button press events.
      //
      Button(broker,  0), Button(broker,  1), Button(broker,  2), Button(broker,  3),
      Button(broker,  4), Button(broker,  5), Button(broker,  6), Button(broker,  7),
      Button(broker,  8), Button(broker,  9), Button(broker, 10), Button(broker, 11),
      Button(broker, 12), Button(broker, 13), Button(broker, 14), Button(broker, 15),
      Button(broker, 16), Button(broker, 17), Button(broker, 18), Button(broker, 19),
      Button(broker, 20), Button(broker, 21), Button(broker, 22), Button(broker, 23),
      Button(broker, 24), Button(broker, 25), Button(broker, 26), Button(broker, 27),
      Button(broker, 28), Button(broker, 29), Button(broker, 30), Button(broker, 31),
    })
  {
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  void ButtonModule::begin()
  {
    subscribe(Module::SYSTEM_KEY);

    for (uint8_t i = 0; i < PORT_COUNT; i++)
    {
      _buttonPorts[i].begin(i, _buttonPortPinMap[i], _buttons);
    }
  }

  void ButtonModule::loop()
  {
    if (_status != SystemStatus::Active)
    {
      return;
    }
    
    // Process all ports.
    for (uint8_t i = 0; i < PORT_COUNT; i++)
    {
      _buttonPorts[i].loop();
    }

    // Process only mapped buttons.
    for (uint8_t i = 0; i < _buttonCount; i++)
    {
      _buttons[i].loop();
    }
  }

  void ButtonModule::notify(Message * pMessage)
  {
    ModuleBase::notify(pMessage);
  }
}
