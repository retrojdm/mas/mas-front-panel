#pragma once

#include <Arduino.h>
#include <PubSub.h>
#include <MasCore.h>
#include "../../Registers.h"
#include "Button.h"

namespace mas
{ 
  // Button Port
  // ===========
  //
  // Because we're stepping through each port, it's cleaner to use a "Button Port" handler, rather than cramming all the
  // code in the ButtonModule.
  //
  // Essentially, this object will:
  //  1.  Read the port (an entire byte representing 8 pins)
  //  2.  Shift through each bit, and if the bit position maps to a button object, set the state of that button.

  // Class definition //////////////////////////////////////////////////////////////////////////////////////////////////

  class ButtonPort
  {
  public:
    ButtonPort();
    void begin(uint8_t port, const uint8_t buttonMap[8], Button *buttons);
    void loop();

  private:
      uint8_t           _port;
      bool              _enabled;
      volatile uint8_t  *_pDDRx;
      volatile uint8_t  *_pPORTx;
      volatile uint8_t  *_pPINx;
      Button            *_pButtonMap[8]; // Array of pointers to button objects. nullptr = no button mapped to the pin.
  };
}
