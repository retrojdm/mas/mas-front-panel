#pragma once

#include <Arduino.h>
#include <PubSub.h>
#include <MasCore.h>
#include "ButtonPort.h"
#include "Button.h"

namespace mas
{
  // Button Module
  // =============
  //
  // We're using direct port manipulation instead of digitalRead() etc.
  //
  // See src/Register.h for an explanation about why we're doing it this way.
  //
  // So, we need to invert our thinking from what we're used to. Instead of reading a pin by it's Arduino pin number, we
  // start by reading a byte from one of the four ports, then shifting through all eight bits. If the bit position maps
  // to a button object, we update it's state.

  // Constants /////////////////////////////////////////////////////////////////////////////////////////////////////////

  const uint8_t BUTTONS_MAX = PORT_COUNT * 8;

  // Class definition //////////////////////////////////////////////////////////////////////////////////////////////////

  class ButtonModule : public ModuleBase
  {
  public:
    ButtonModule(
      Broker<Message *> & broker,
      char* const errorBuffer,
      const uint8_t (*buttonPortPinMap)[8],
      const uint8_t buttonCount);

    void begin();
    void loop();
    void notify(Message * pMessage) override;

  private:
    void _initPort(uint8_t portIndex);
    void _readPort(uint8_t portIndex);

    const uint8_t (*_buttonPortPinMap)[8];
    uint8_t _buttonCount;
    ButtonPort _buttonPorts[PORT_COUNT];
    Button _buttons[BUTTONS_MAX];
  };
}
