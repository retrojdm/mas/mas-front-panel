#include "EncoderModule.h"

namespace mas
{
  // Class constructor /////////////////////////////////////////////////////////////////////////////////////////////////

  EncoderModule::EncoderModule(Broker<Message *> & broker, char* const errorBuffer) :
    ModuleBase(broker, Module::ENCODER_KEY, errorBuffer),
    _encoder({

      // Don't change the index values.
      Encoder(broker, 0, ENCODER_LEFT_PORT, ENCODER_LEFT_PIN_A, ENCODER_LEFT_PIN_B),
      Encoder(broker, 1, ENCODER_RIGHT_PORT, ENCODER_RIGHT_PIN_A, ENCODER_RIGHT_PIN_B),
    })
  {
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  void EncoderModule::begin()
  {
    subscribe(Module::SYSTEM_KEY);

    for (uint8_t i = 0; i < ENCODER_COUNT; i++)
    {
      _encoder[i].begin();
    }
  }

  void EncoderModule::loop()
  {
    if (_status != SystemStatus::Active)
    {
      return;
    }
    
    for (uint8_t i = 0; i < ENCODER_COUNT; i++)
    {
      _encoder[i].loop();
    }
  }

  void EncoderModule::notify(Message * pMessage)
  {
    ModuleBase::notify(pMessage);
  }

  void EncoderModule::isr(uint8_t encoder)
  {
    _encoder[encoder].isr();
  }
}