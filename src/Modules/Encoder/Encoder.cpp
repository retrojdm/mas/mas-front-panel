#include "Encoder.h"

namespace mas
{
  // Class constructor /////////////////////////////////////////////////////////////////////////////////////////////////

  Encoder::Encoder(
    Broker<Message *> & broker,
    uint8_t index,
    uint8_t port,
    uint8_t portPinA, 
    uint8_t portPinB)
    :
    Publisher(broker)
  {
    _index    = index;
    _pPINx    = PINx[port]; // Store the port address for faster code in the ISR.
    _port     = port;
    _portPinA = portPinA;
    _portPinB = portPinB;
    _state    = 0;
  }

  // Private functions /////////////////////////////////////////////////////////////////////////////////////////////////

  void Encoder::begin()
  {
    // We only use these in the begin() function here, so there's no need to remember them as a member parameter.
    volatile uint8_t *pDDRx    = DDRx[_port];
    volatile uint8_t *pPORTx   = PORTx[_port];
    volatile uint8_t *pPCMSKx  = PCMSKx[_port];

    uint8_t mask = _BV(_portPinA) | _BV(_portPinB);
   
    noInterrupts();
    *pDDRx  &= ~mask;     // Pins goes low (set as input).
    *pPORTx |= mask;      // Set pullup resistors.
    *pPCMSKx |= mask;     // Enable pin change detection, only for the encoder pins.
    PCICR |= _BV(_port);  // Enable pin change interrupts for the port.
    interrupts();

    // Get current state of pins.
    _state = _readPins();
  }

  // We only publish a single increment/decrement per loop.
  //
  // We look at the value of _count (which is updated externally by the ISR).
  //
  // _count's distance from zero determines how far, and in which direction we've turned.
  //
  void Encoder::loop()
  {
    // Incremented?
    if (_count >= ENCODER_PULSES_PER_DETENT)
    {
      // Adjust the count closer to zero.
      _count -= ENCODER_PULSES_PER_DETENT;

      publish(new(std::nothrow) EncoderChangedEvent(_index, +1));
      return;
    }

    // Decremented?
    if (_count <= -ENCODER_PULSES_PER_DETENT)
    {
      // Adjust the count closer to zero.
      _count += ENCODER_PULSES_PER_DETENT;

      publish(new(std::nothrow) EncoderChangedEvent(_index, -1));
      return;
    }   
  }

  void Encoder::isr()
  {
    // Shift the old state up, and set the lower two bits to the current state.
    //
    // The lower four bits should now correspond to the following:
    // 
    // bit 0 = current A pin
    // bit 1 = current B pin
    // bit 2 = previous A pin
    // bit 3 = previous B pin
    //    
    _state <<= 2;
    _state |= _readPins();
    _state &= 0b00001111;
    _count += ENCODER_STATE_MAP[_state];
  }

  // Private functions /////////////////////////////////////////////////////////////////////////////////////////////////

  // We read the A and B pins into a two-bit value:
  //
  // bit 0 = A pin
  // bit 1 = B pin
  //
  uint8_t Encoder::_readPins()
  {
    uint8_t portValue = *_pPINx;
    uint8_t a = (portValue>>_portPinA) & 1;
    uint8_t b = (portValue>>_portPinB) & 1;
    return (b<<1)|a;
  }
}