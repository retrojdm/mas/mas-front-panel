#pragma once

#include <Arduino.h>
#include <PubSub.h>
#include <MasCore.h>
#include "../../Registers.h"

namespace mas
{  
  // Encoder
  // =======
  //
  // We step through each Encoder object in the EncoderModule's loop() function.
  //
  // The encoder's loop() function will publish increment/decrement events depending on the distance from zero of the
  // _count variable.
  //
  // The _count variable is updated by the "Pin Change" Interrupt Service Routine (ISR), which is hard-coded in the main
  // .ino file.
  //
  // The ISR is hard-coded because it's much simpler than trying to dynamically set up a member function as an ISR.
  //
  // See EncoderModule.h for more info on how all this works.

  // Constants /////////////////////////////////////////////////////////////////////////////////////////////////////////

  // How many pulses to accumulate in one direction before queing an 'incremented' or 'decremented' event?
  static const int8_t ENCODER_PULSES_PER_DETENT = 4;

  // With a 4-bit value, there are 16 possible combinations.
  // We can map each to either an increment, decrement, or no change.
  static const int8_t ENCODER_STATE_MAP[16] = { 0, 1, -1, 0, -1, 0, 0, 1, 1, 0, 0, -1, 0, -1, 1, 0 };
  
  // Class definition //////////////////////////////////////////////////////////////////////////////////////////////////

  class Encoder : public pubsub::Publisher<Message *>
  {
  public:
    Encoder(
      Broker<Message *> & broker,
      uint8_t index,
      uint8_t port,
      uint8_t portPinA,
      uint8_t portPinB);

    void begin ();
    void loop ();
    void isr ();

  private:
    uint8_t               _index;
    volatile uint8_t *    _pPINx;
    uint8_t               _port;
    uint8_t               _portPinA;
    uint8_t               _portPinB;
    volatile int8_t       _count; // updated by the ISR.
    volatile uint8_t      _state; // updated by the ISR.

    uint8_t _readPins();
  };
}