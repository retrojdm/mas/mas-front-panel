#pragma once

#include <Arduino.h>
#include <PubSub.h>
#include <MasCore.h>
#include "Encoder.h"

namespace mas
{
  // Encoder Module
  // ==============
  //
  // The ATMEGA1284 only has three external interrupts - not enough to reliably read two rotary encoders.
  //
  // However, it lets us set up a "Pin Change Interrupt" for each of it's four ports: A, B, C, and D.
  //
  // The name's a little misleading though. An interrupt is actually invoked when _any_ enabled pin on the port changes.
  // Luckily this is all we need if we're careful with our pin choices.
  //
  // We can connect each encoder to a separate port, and only enable pin change detection for the encoder's pins
  // (ignoring changes to the rest of the port).

  // Constants /////////////////////////////////////////////////////////////////////////////////////////////////////////

  const uint8_t ENCODER_COUNT = 2;

  // Left
  const uint8_t ENCODER_LEFT_PORT = PORT_B;
  const uint8_t ENCODER_LEFT_PIN_A = 0; // PB4
  const uint8_t ENCODER_LEFT_PIN_B = 1; // PB3

  // Right
  const uint8_t ENCODER_RIGHT_PORT = PORT_C;
  const uint8_t ENCODER_RIGHT_PIN_A = 2; // PC2
  const uint8_t ENCODER_RIGHT_PIN_B = 3; // PC3

  // Class definition //////////////////////////////////////////////////////////////////////////////////////////////////

  class EncoderModule : public ModuleBase
  {
  public:
    EncoderModule(Broker<Message *> & broker, char* const errorBuffer);
    void begin();
    void loop();
    void notify(Message * pMessage) override;
    void isr(uint8_t encoder);

  private:
    Encoder _encoder[ENCODER_COUNT];
  };
}