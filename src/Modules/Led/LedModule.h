#pragma once

#include <Arduino.h>
#include <PubSub.h>
#include <MasCore.h>
#include "Led.h"

namespace mas
{
  // Led Module
  // ==========
  //
  // Controls all six PWM LEDs on the FP10.
  //
  // TODO: Use ports instead of analogWrite()

  // Constants /////////////////////////////////////////////////////////////////////////////////////////////////////////

  const uint8_t LED_COUNT = 6;       // PD4, PD5, PD6, PD7, PB4, PB3
  const uint8_t LED_PINS[LED_COUNT] = {  12,  13,  14,  15,   4,   3 };
  
  // Class definition //////////////////////////////////////////////////////////////////////////////////////////////////

  class LedModule : public ModuleBase
  {
  public:
    LedModule(Broker<Message *> & broker, char* const errorBuffer);
    void begin();
    void loop();
    void notify(Message * pMessage) override;
    void onStop();
    
  private:
    void _onLedLevelsCommand(LedLevelsCommand * pCommand);

    Led _leds[LED_COUNT];
  };
}
