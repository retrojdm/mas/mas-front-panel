#pragma once

#include <Arduino.h>

#include "../../Registers.h"

namespace mas
{
  // Constants /////////////////////////////////////////////////////////////////////////////////////////////////////////

  // We're using analogWrite() to control our PWM LEDs, which takes a value from 0 to 255.
  const uint8_t LED_MAX_BRIGHTNESS = 255;

  // We map to an exponential scale, because very small changes at the bottom of the scale make a far more noticable
  // differece in brightness.
  // 1.0 = linear.
  const double LED_EXPONENT = 3.0;

  // Class definition //////////////////////////////////////////////////////////////////////////////////////////////////

  class Led
  {
  public:
    Led();
    void begin(const uint8_t pin);
    void setLevel(const uint8_t level);

  	uint8_t level;

  private:
    uint8_t _pin;
  };
}
