#include "Led.h"

namespace mas
{
  // Class constructor /////////////////////////////////////////////////////////////////////////////////////////////////

  Led::Led()
  {
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  void Led::begin(const uint8_t pin)
  {
    _pin = pin;    
    level = 0;
    
    setLevel(level);
  }

  void Led::setLevel(const uint8_t level)
  {
    // Attenuate the lower values, since the LED appears too bright, even at low duty cycles when using a linear scale.
    double scaledInput = static_cast<double>(level) / LED_MAX_BRIGHTNESS;
    double exponentialResult = pow(scaledInput, LED_EXPONENT);
    uint8_t pwmDutyCycle = exponentialResult * LED_MAX_BRIGHTNESS;

    analogWrite(_pin, pwmDutyCycle);
    this->level = level;
  }
}
