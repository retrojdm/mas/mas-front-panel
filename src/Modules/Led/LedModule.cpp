#include "LedModule.h"

namespace mas
{
  // Class constructors ////////////////////////////////////////////////////////////////////////////////////////////////

  LedModule::LedModule(Broker<Message *> & broker, char* const errorBuffer)
  : ModuleBase(broker, Module::LED_KEY, errorBuffer)
  {
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  void LedModule::begin()
  {
    subscribe(Module::SYSTEM_KEY);
    subscribe(Module::LED_KEY);

    for (uint8_t i = 0; i < LED_COUNT; i++)
    {
      _leds[i].begin(LED_PINS[i]);
    }
  }

  void LedModule::loop()
  {
  }

  void LedModule::notify(Message * pMessage)
  {
    ModuleBase::notify(pMessage);

    if (_status != SystemStatus::Active)
    {
      return;
    }

    switch (pMessage->type())
    {
    case MessageType::LedLevelsCommand:
      _onLedLevelsCommand(static_cast<LedLevelsCommand *>(pMessage));
      break;

    default:
      break;
    }
  }

  void LedModule::onStop()
  {
    for (uint8_t i = 0; i < LED_COUNT; i++)
    {
      _leds[i].setLevel(0);
    }

    _status = SystemStatus::Stopped;
  }

  // Private functions /////////////////////////////////////////////////////////////////////////////////////////////////

  // LED <levels[]>
  // Get or set comma-separated list of LED levels.
  void LedModule::_onLedLevelsCommand(LedLevelsCommand * pCommand)
  {
    if (pCommand->levels.length() > 0)
    {
      for (uint8_t i = 0; i < min(pCommand->levels.length(), LED_COUNT); i++)
      {
        _leds->setLevel(pCommand->levels[i]);
      }
    }

    uint8_t levelsBuffer[LED_COUNT];
    Array<uint8_t> levels(levelsBuffer, LED_COUNT);
    for (uint8_t i = 0; i < LED_COUNT; i++)
    {
      levels[i] = _leds[i].level;
    }

    publish(new(std::nothrow) LedLevelsCommand(levels));
  }
}
