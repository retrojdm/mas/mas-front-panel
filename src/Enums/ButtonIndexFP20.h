#pragma once

#include <Arduino.h>

namespace mas
{
  // How many buttons in total?
  const uint8_t BUTTON_COUNT_FP20 = 7;

  enum class ButtonIndexFP20:uint8_t
  {
    EncLeft     = 0,
    EncRight    = 1,
    MenuOk      = 2,
    PlayPause   = 3,
    Prev        = 4,
    Next        = 5,
    FolderBack  = 6,

    __COUNT,
  };
}
