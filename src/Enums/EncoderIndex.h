#pragma once

namespace mas
{
  // IMPORTANT
  //
  // Don't change the order of these without also changing their order in the EncoderModule constructor.
  //
  // See EncoderModule.cpp
  //
  enum class EncoderIndex:uint8_t
  {
    Left  = 0,
    Right = 1,

    __COUNT,
  };
}