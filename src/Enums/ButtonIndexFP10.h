#pragma once

#include <Arduino.h>

namespace mas
{
  // How many buttons in total?
  const uint8_t BUTTON_COUNT_FP10 = 14;

  enum class ButtonIndexFP10:uint8_t
  {
    EncLeft   =  0,
    EncRight  =  1,
    Display   =  2,
    Mute      =  3,
    MenuOk    =  4,
    Back      =  5,
    P1Play    =  6,
    P2Pause   =  7,
    P3Stop    =  8,
    P4Shuffle =  9,
    P5Repeat  = 10,
    Prev      = 11,
    Next      = 12,
    P6Folder  = 13,

    __COUNT,
  };
}
