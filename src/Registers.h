#pragma once

// Direct Port Manipulation Helper
// ===============================
//
// We're not using the Arduino IO functions (pinMode/digitalRead/digitalWrite) anywhere in the front panel. There's a
// lot of overhead in the Arduino library to make these functions work on multiple boards, which we don't need because
// we know we're only ever going to compile this code for an ATmega1284P.
//
// Directly manipulating ports speeds the code up significantly, allowing for more responsive rotary encoders, buttons,
// LEDs, and a faster frame rate for the display.
//
// This choice comes at the expense of code readability, portability, and ease of debugging, but the performance
// increases were considered worth it.
//
// In an attempt to keep the code as readable and maintainable as possible, the arrays defined below are used to allow
// ports and pins to be defined in each module's header files, rather than hard-coding things.


// Ports
const uint8_t PORT_COUNT = 4;

const uint8_t PORT_A = 0;
const uint8_t PORT_B = 1;
const uint8_t PORT_C = 2;
const uint8_t PORT_D = 3;

// Index to use when the port pin is not mapped to a button. Can't be zero.
const uint8_t BUTTON_PORT_PIN_NOT_MAPPED = 255;

// Data Direction Registers
// Allows us to set the pinmode. 0 = input. 1 = output.
volatile uint8_t * const DDRx[] = { &DDRA, &DDRB, &DDRC, &DDRD };

// Port Data Registers
// Allows us to write the IO pins when pinmode is set to output, or enable 
// the internal pullup resistors when pinmode is set to input.
volatile uint8_t * const PORTx[] = { &PORTA, &PORTB, &PORTC, &PORTD };

// Port Input Registers
// Allows us to read the IO pins when pinmode is set to input.
volatile uint8_t * const PINx[] = { &PINA, &PINB, &PINC, &PIND };

// Pin Change Interrupt Masks
// Allows us to set pin change detection for each pin on each port.
volatile uint8_t * const PCMSKx[] = { &PCMSK0, &PCMSK1, &PCMSK2, &PCMSK3 };
