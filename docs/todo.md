# TODO

## Decouple UI Module from u8g2

Create display interface in `mas-front-panel` that OLED, VFD, and LED variants can all use.

## Move common front-panel code

Move common front-panel code to `mas-front-panel`. Eg: `ScreenBase` and `ControlBase` from FP20-OLED.