# Modular Audio System (MAS) Front Panel Arduino Library

## ⚠️ Work In Progress

**This project is still in development and is untested.**

---

Part of the retrojdm.com _Modular Audio System_.

|     | Repo                                                                   | Description                                  |
| :-: | :--------------------------------------------------------------------- | :------------------------------------------- |
|     | [mas-core](https://gitlab.com/retrojdm/mas/mas-core)                   | Core Arduino library for firmware            |
|  🡆  | **[mas-front-panel](https://gitlab.com/retrojdm/mas/mas-front-panel)** | **Front Panel Arduino library for firmware** |
|     | [MU50](https://gitlab.com/retrojdm/mas/MU50)                           | Main Unit and I/O cards                      |
|     | [FP10](https://gitlab.com/retrojdm/mas/FP10)                           | 12-button Front Panel                        |
|     | [FP20](https://gitlab.com/retrojdm/mas/FP20)                           | 5-button Front Panel                         |

---

This library contains classes (including the Button and Encoder modules), enums, and constants common to Front Panel units (i.e. [FP10](https://gitlab.com/retrojdm/mas/FP10) and [FP20](https://gitlab.com/retrojdm/mas/FP20)).