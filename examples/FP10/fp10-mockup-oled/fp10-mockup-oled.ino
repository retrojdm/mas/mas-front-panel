// This is a UI mock-up of the FP10 OLED.
// The real UI will use fonts instead of copying bitmap data, and _should_ be fast enough to scroll
// text and animate the spectrum analyser. I guess we'll see...

#include <Arduino.h>
#include <U8g2lib.h>
#include <SPI.h>

#define WIDTH 256
#define HEIGHT 32

#define OLED_CS_PIN 18
#define OLED_DC_PIN 16
#define OLED_RESET_PIN 17
#define LED5_PIN 4

// Use the hardware SPI constructor. it's MUCH faster than the software SPI variants.
// If you don't have enough RAM, try the _1_ or _2_ paging variants instead of this _F_ full frame one.
// Note: a full frame at 256 x 32 x 1-bit = 1K of RAM.
// See https://github.com/olikraus/u8g2/wiki/u8g2setupcpp#buffer-size

// FP10-PCB-OLE:
U8G2_SSD1326_ER_256X32_F_4W_HW_SPI u8g2(U8G2_R0, OLED_CS_PIN, OLED_DC_PIN, OLED_RESET_PIN);

unsigned long start = 0;
int count = 0;

void setup(void)
{
  u8g2_uint_t width = u8g2.getDisplayWidth();
  Serial.begin(100000);
  Serial.print("Width: ");
  Serial.println(width);

  // We don't used the Slave Select (SS) pin, since the ATMEGA1284 is always in master mode.
  // By default it's pulled high, but since the pin is connected to LED5, we want to switch it off in this mockup.
  pinMode(LED5_PIN, OUTPUT);
  digitalWrite(LED5_PIN, LOW);
  
  randomSeed(analogRead(0));
  u8g2.begin();
  renderTitle();
  renderArtistAlbumYear();
  renderTrackNumberTime();

  start = millis();
}

void loop(void)
{
  renderSpectrumAnalyser();

  // See https://github.com/olikraus/u8g2/wiki/u8g2reference#updatedisplayarea
  //
  // We have to render the entire display, so we're limited to around 20 FPS.
  // `updateDisplayArea(tx, ty, tw, th)` sort of works on the SSD1326 display, but it clears the non-updated area.
  //
  u8g2.updateDisplay();

  count++;
  if (count == 100)
  {
    unsigned long now = millis();
    float durationPerFrameInSeconds = (now - start) / 100000.0;

    float fps = 1.0 / durationPerFrameInSeconds;
    Serial.print("FPS: ");
    Serial.println(fps);
    count = 0;
    start = now;
  }
}

void renderTitle()
{
  // Title
  u8g2.setClipWindow(0, 0, WIDTH - 64 - 1, 19);
  u8g2.setFont(u8g2_font_helvB14_tr);
  u8g2.drawStr(0, 15, "Stairway to Heaven");
}

void renderArtistAlbumYear()
{ 
  // Artist - Album (year)
  u8g2.setClipWindow(0, 20, WIDTH - 64 - 1, 31);
  u8g2.setFont(u8g2_font_6x13_tr);
  u8g2.drawStr(0, 29, "Led Zeppelin - Remasters (1990)");
}

void renderTrackNumberTime()
{ 
  // Track No.
  u8g2.setMaxClipWindow();
  u8g2.setFont(u8g2_font_6x13_tn);
  u8g2.drawStr(WIDTH - 64, 9, "15/26");

  // Time
  const char* s = "1:45";
  u8g2.drawStr(WIDTH - u8g2.getStrWidth(s), 9, s);
}

void renderSpectrumAnalyser()
{ 
  const byte barCount = 8;
  const byte barWidth = 8;
  const byte barHeight = 20;
  const byte x0 = WIDTH - (barWidth * barCount);
  const byte y0 = HEIGHT - barHeight;

  u8g2.setDrawColor(0);
  u8g2.drawBox(x0, y0, barWidth * barCount, barHeight);
  u8g2.setDrawColor(1);
  
  for (int i = 0; i < barCount; i++)
  {
    int h = random(256) * barHeight / 256;
    int x = i * barWidth + x0;
    int y = HEIGHT - 1 - h;
    u8g2.drawBox(x, y, barWidth - 1, h);
  }
}
