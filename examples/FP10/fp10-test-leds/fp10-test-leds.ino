// Test LEDs example program ///////////////////////////////////////////////////////////////////////////////////
//
// FP10 Front Panel
// Modular Audio System (MAS)
// retrojdm.com
//
// "Scans" the LEDs left to right like K.I.T.T. from KnightRider, a Cylon from Battlestar Galactica, or a metronome.
//
// https://gitlab.com/retrojdm/mas/FP10

// Libraries ///////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <Arduino.h>
#include <RingBuffer.h>
#include <PubSub.h>
#include <MasCore.h>
#include <MasFP.h>
#include <MasFP10.h>

// Front panel modules /////////////////////////////////////////////////////////////////////////////////////////////////

#include "src/KittModule.h"

using namespace mas;

Message messageBuffer[DataSize::MESSAGES_MAX];
Subscription<Message> subscriptionsBuffer[DataSize::SUBSCRIPTIONS_MAX];

// See maslib/src/Message.h
Broker<Message> broker(
  messageBuffer,
  DataSize::MESSAGES_MAX,
  subscriptionsBuffer,
  DataSize::SUBSCRIPTIONS_MAX,

  // Topics are derived from messages as the first character of the code.
  [] (Message message) { return static_cast<uint8_t>(message.code[0]); },

  // If the broker has an overflow error, we display an error message then halt the program.
  [] (char* error) { Serial.print("[SLG] E - \"FP20: "); Serial.print(error); Serial.println("\""); while (true) {} });

// Modules /////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Note:  The Serial module (part of the mas-core library) must be first, so that it's registered to send/receives
//        messages first.
//
const size_t SERIAL_PORT_COUNT = 1;
SerialPort serialPorts[SERIAL_PORT_COUNT] = {
  SerialPort(&broker, &Serial, "Main Unit"),
};

SerialModule  serialModule  (&broker, serialPorts, SERIAL_PORT_COUNT);
LedModule     ledModule     (&broker);
KittModule    kittModule    (&broker);

// Main program ////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup()
{
  // We can't do this within the Serial module's .begin() method, because we're passing pointers to Stream objects,
  // which don't have a .begin() method.
  //
  Serial.begin(SerialModule::BAUD_RATE); // 115,200 by default. See mas-core/src/Serial/SerialModule.h

  serialModule.begin();
  ledModule.begin();
  kittModule.begin();
}

void loop()
{
  serialModule.loop();
  ledModule.loop();
  kittModule.loop();
  broker.distribute();
}
