#include "KittModule.h"

namespace mas
{
  // Constructor ///////////////////////////////////////////////////////////////////////////////////////////////////////

  KittModule::KittModule(Broker<Message> *pBroker) : ModuleBase(this, pBroker, Module::UI_KEY)
  {
    _nextTick = 0;
    _bufferUpdated = false;
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////
  
  void KittModule::begin()
  {
    subscribe(Module::SYSTEM_KEY);

    _nextTick = millis();
  }

  void KittModule::loop()
  {
    while (millis() > _nextTick)
    {
        _update();
        _nextTick += KITT_UPDATE_FRAME_DELAY;
    }

    _render();
  }

  // Private functions /////////////////////////////////////////////////////////////////////////////////////////////////

  // Update the "physics" for the peak drops.
  void KittModule::_update()
  { 
    _degrees = fmod((_degrees + KITT_DEGREES_PER_FRAME), 360.0);
    _bufferUpdated = true;
  }

  // Refresh the display.
  void KittModule::_render()
  {    
    if (!_bufferUpdated)
    {
      return;
    }

    _bufferUpdated = false;
    Message command(MessageType::Command, CodeFP::LED_LEVELS);

    for (int16_t i = 0; i < LED_COUNT; i++)
    {
      double radians = _degrees * (PI / 180.0);
      double x = sin(radians) * KITT_RADIUS;

      int16_t level =
        LED_MAX_BRIGHTNESS - abs((double)(x - KITT_LED_POSITIONS[i]) / KITT_GLOW_WIDTH * LED_MAX_BRIGHTNESS);

      command.addParameter(level > 0 ? level : 0);
    }

    publish(command);
  }
}
