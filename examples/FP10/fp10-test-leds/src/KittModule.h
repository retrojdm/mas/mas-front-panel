#pragma once

#include <PubSub.h>
#include <MasCore.h>
#include <MasFP.h>
#include <MasFP10.h>

namespace mas
{
  static const int16_t KITT_DEGREES_PER_SECOND = 180;
  static const int8_t KITT_FPS = 20;
  static const double KITT_GLOW_WIDTH = 25; // mm (can't be zero)
  static const double KITT_RADIUS = 35.5; // mm

  static const int16_t KITT_DEGREES_PER_FRAME = KITT_DEGREES_PER_SECOND / KITT_FPS;
  static const int16_t KITT_UPDATE_FRAME_DELAY = 1000 / KITT_FPS;

  // On the FP10, there's a large gap between LED 5 and 6 where the Prev/Next buttons are.
  static const double KITT_LED_POSITIONS[LED_COUNT] = { -35.5, -25.75, -16, -6.25, 3.5, 35.5 }; // mm
  
  class KittModule : public ModuleBase<KittModule>
  {
  public:
    KittModule(Broker<Message> *pBroker);
    void begin();
    void loop();

  private:
    void _update();
    void _render();

    double _degrees = 0; // degrees
    uint32_t _nextTick = 0;
    bool _bufferUpdated = false;
  };
}
