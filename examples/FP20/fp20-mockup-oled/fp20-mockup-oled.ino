// This is a UI mock-up of the FP20 OLED.
// The real UI will use fonts instead of copying bitmap data, and _should_ be fast enough to scroll
// text and animate the spectrum analyser. I guess we'll see...

#include <Arduino.h>
#include <U8g2lib.h>
#include <SPI.h>

// To get 256px wide instead of 240px, search for and uncomment the following line in "u8g2.h":
//    #define U8G2_16BIT
//
#define WIDTH 256
#define HEIGHT 64

// Use the hardware SPI constructor. it's MUCH faster than the software SPI variants.
// If you don't have enough RAM, try the _1_ or _2_ paging variants instead of this _F_ full frame one.
// Note: a full frame at 256 x 32 x 1-bit = 1K of RAM.
// See https://github.com/olikraus/u8g2/wiki/u8g2setupcpp#buffer-size

// FP20-PCB-MBD:
U8G2_SH1122_256X64_F_4W_HW_SPI u8g2(U8G2_R0, /* cs=*/ 21, /* dc=*/ 22, /* reset=*/ 23);

unsigned long start = 0;
int count = 0;

void setup(void)
{
  u8g2_uint_t width = u8g2.getDisplayWidth();
  Serial.begin(100000);
  Serial.print("Width: ");
  Serial.println(width);
  
  randomSeed(analogRead(0));
  u8g2.begin();
  renderTitle();
  renderArtistAlbumYear();
  renderTrackNumberTime();

  start = millis();
}

void loop(void)
{
  renderSpectrumAnalyser();
  
  // See https://github.com/olikraus/u8g2/wiki/u8g2reference#updatedisplayarea
  //
  // We have to render the entire display, so we're limited to around 10 FPS.
  // `updateDisplayArea(tx, ty, tw, th)` doesn't work on the SH1122 display.
  // 
  u8g2.updateDisplay();

  count++;
  if (count == 100)
  {
    unsigned long now = millis();
    float durationPerFrameInSeconds = (now - start) / 100000.0;

    float fps = 1.0 / durationPerFrameInSeconds;
    Serial.print("FPS: ");
    Serial.println(fps);
    count = 0;
    start = now;
  }
}

void renderTitle()
{
  // Title
  u8g2.setClipWindow(0, 0, WIDTH - 64 - 1, HEIGHT / 2 - 1);
  u8g2.setFont(u8g2_font_luRS18_tr);
  u8g2.drawStr(0, 18, "Stairway to Heaven");
}

void renderArtistAlbumYear()
{ 
  // Artist
  // Album (year)
  u8g2.setClipWindow(0, 20, WIDTH - 64 - 1, HEIGHT - 1);
  u8g2.setFont(u8g2_font_9x15_tr);
  u8g2.drawStr(0, HEIGHT - 24, "Led Zeppelin");
  u8g2.drawStr(0, HEIGHT - 4, "Remasters (1990)");
}

void renderTrackNumberTime()
{ 
  u8g2.setMaxClipWindow();
  u8g2.setFont(u8g2_font_9x15_tn);

  // Track No.
  const char* track = "15/26";
  u8g2.drawStr(WIDTH - u8g2.getStrWidth(track), 10, track);

  // Time
  const char* time = "1:45";
  u8g2.drawStr(WIDTH - u8g2.getStrWidth(time), 24, time);
}

void renderSpectrumAnalyser()
{ 
  const byte barCount = 8;
  const byte barWidth = 8;
  const byte barHeight = 32;
  const byte x0 = WIDTH - (barWidth * barCount);
  const byte y0 = HEIGHT - barHeight;

  u8g2.setDrawColor(0);
  u8g2.drawBox(x0, y0, barWidth * barCount, barHeight);
  u8g2.setDrawColor(1);
  
  for (int i = 0; i < barCount; i++)
  {
    int h = random(256) * barHeight / 256;
    int x = i * barWidth + x0;
    int y = HEIGHT - 1 - h;
    u8g2.drawBox(x, y, barWidth - 1, h);
  }
}