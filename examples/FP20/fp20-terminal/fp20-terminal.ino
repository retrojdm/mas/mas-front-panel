// Terminal example program ////////////////////////////////////////////////////////////////////////////////////////////
//
// FP20 Front Panel
// Modular Audio System (MAS)
// retrojdm.com
//
// Simply echos messages to the OLED display.
//
// https://gitlab.com/retrojdm/mas/FP20

// Libraries ///////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <SPI.h>
#include <Arduino.h>
#include <U8g2lib.h>

#include <TimeLib.h>      // https://www.pjrc.com/teensy/td_download.html

#include <RingBuffer.h>   // https://gitlab.com/retrojdm/ring-buffer
#include <PubSub.h>       // https://gitlab.com/retrojdm/pub-sub
#include <MasCore.h>       // https://gitlab.com/retrojdm/mas/lib

// Front panel modules /////////////////////////////////////////////////////////////////////////////////////////////////
//
// See each module's header file for more detailed info.

#include "src/Terminal/TerminalModule.h"

using namespace mas;

Message messageBuffer[DataSize::MESSAGES_MAX];
Subscription<Message> subscriptionsBuffer[DataSize::SUBSCRIPTIONS_MAX];

// See maslib/src/Message.h
Broker<Message> broker(
  messageBuffer,
  DataSize::MESSAGES_MAX,
  subscriptionsBuffer,
  DataSize::SUBSCRIPTIONS_MAX,
  [] (Message message) { return static_cast<uint8_t>(message.code[0]); },
  [] (char* error) { Serial.print("[SLG] E - \"FP20: "); Serial.print(error); Serial.println("\""); while (true) {} });

// Modules /////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Note:  The Serial module (part of the Maslib library) must be first, so that it's registered to send/receives
//        messages first.
//
const size_t PORT_COUNT = 1;
Port ports[PORT_COUNT] = {
  Port(&broker, &Serial, "Main Unit"),
};

SerialModule    serialModule    (&broker, ports, PORT_COUNT);
TerminalModule  terminalModule  (&broker);

// Main program ////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup()
{
  // We can't do this within the Serial module's .begin() method, because we're passing pointers to Stream objects,
  // which don't have a .begin() method.
  //
  Serial.begin(SerialModule::BAUD_RATE); // 115,200 by default. See maslib/src/Serial/SerialModule.h
  
  serialModule.begin();
  terminalModule.begin();
}

void loop()
{
  serialModule.loop();
  terminalModule.loop();
  broker.distribute();
}
