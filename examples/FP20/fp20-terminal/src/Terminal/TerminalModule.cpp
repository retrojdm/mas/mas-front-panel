#include "TerminalModule.h"

namespace mas
{
  // Constructor ///////////////////////////////////////////////////////////////////////////////////////////////////////

  TerminalModule::TerminalModule(Broker<Message> *pBroker)
    : ModuleBase(this, pBroker, Module::UI_KEY), _u8g2(U8G2_R0, CS_PIN, DC_PIN, RESET_PIN)
  {
    _lineOffset = 0;
    
    for (uint8_t i = 0; i < LINES; i++)
    {
      _lines[i][0] = '\0';
    }

    _renderRequired = false;
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  void TerminalModule::begin()
  {
    _u8g2.begin();
    _u8g2.setFont(u8g2_font_5x7_tr);
    _u8g2.clear();

    subscribe(Module::ALL_KEY);
  }

  void TerminalModule::loop()
  {
    if (_renderRequired)
    {
      _render();
    }
  }

  // Called when the broker notifies this module about a message it's subscribed to.
  void TerminalModule::notify(Message message)
  {
    _lineOffset = (_lineOffset + 1) % LINES;
    SerialisationHelper::serialise(message, _lines[_lineOffset]);
    _renderRequired = true;
  }
  
  // This is the actual render.
  void TerminalModule::_render()
  {
    _renderRequired = false;
    _u8g2.clearBuffer();

    // Row numbers start at top.
    for (uint8_t row = 0; row < LINES; row++)
    {
      // Line numbers start at bottom.
      // This means we scrool from the bootom up.
      _printLine(LINES - row - 1);
    }

    _u8g2.sendBuffer();
  }

  // Prints the i-th line of the display.
  void TerminalModule::_printLine(uint8_t i)
  {
    uint8_t j = (_lineOffset + LINES - i) % LINES;
    _u8g2.drawStr(0, DISPLAY_HEIGHT - LINE_BASE - (i * LINE_HEIGHT), _lines[j]);
  }
}
