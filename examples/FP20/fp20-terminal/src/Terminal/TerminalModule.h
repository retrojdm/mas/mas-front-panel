#pragma once

#include <U8g2lib.h>
#include <PubSub.h>
#include <MasCore.h>

namespace mas
{
  class TerminalModule : public ModuleBase<TerminalModule>
  {
  public:
    TerminalModule(Broker<Message> *pBroker);
    void begin();
    void loop();
    void notify(Message message) override;

  private:
    static const uint8_t CS_PIN = 21;
    static const uint8_t DC_PIN = 22;
    static const uint8_t RESET_PIN = 23;

    // To get 256px wide instead of 240px, search for and uncomment the following line in "u8g2.h":
    //    #define U8G2_16BIT
    //
    static const uint16_t DISPLAY_WIDTH = 256;
    static const uint8_t DISPLAY_HEIGHT = 64;
    static const uint8_t LINES = 8;
    static const uint8_t LINE_HEIGHT = DISPLAY_HEIGHT / LINES;
    static const uint8_t LINE_BASE = 0;

    void _handleRefresh();
    void _render();
    void _printLine(uint8_t i);
    void _printFps();

    U8G2_SH1122_256X64_F_4W_HW_SPI _u8g2;

    char _lines[LINES][SerialisationHelper::SERIALISATION_BUFFER];
    uint8_t _lineOffset;
    bool _renderRequired;
  };
}
